_GitLab University (?)_

----


# All you need to know about GitLab Pages

## Product

- [Product Webpage](https://pages.gitlab.io)
- [We're Bringing GitLab Pages to CE](https://about.gitlab.com/2016/12/24/were-bringing-gitlab-pages-to-community-edition/)


## Getting Started

- [Comprehensive Step-by-Step Guide](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/)
- GitLab Pages from A to Z
  - [Part 1: Static Sites, Domains, DNS Records, and SSL/TLS Certificates](https://gitlab.com/marcia/drafts/blob/master/pages/pages_part-1.md)
  - [Part 2: Quick Start Guide - Setting Up GitLab Pages](https://gitlab.com/marcia/drafts/blob/master/pages/pages_part-2.md)
      - Video tutorial: [How to Publish a Website with GitLab Pages on GitLab.com: from a forked project](#LINK)
      - Video tutorial: [How to Publish a Website with GitLab Pages on GitLab.com: from scratch](#LINK)
  - [Part 3: Creating and Tweaking `.gitlab-ci.yml` for GitLab Pages](https://gitlab.com/marcia/drafts/blob/master/pages/pages_part-3.md)
- Secure GitLab Pages Custom Domain with SSL/TLS Certificates
  - [Let's Encrypt](https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/)
  - [CloudFlare](https://about.gitlab.com/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/)
  - [StartSSL](https://about.gitlab.com/2016/06/24/secure-gitlab-pages-with-startssl/)
- Static Site Generators - Blog Post Series
  - [SSGs Part 1: Static vs Dynamic Websites](https://about.gitlab.com/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/)
  - [SSGs Part 2: Modern Static Site Generators](https://about.gitlab.com/2016/06/10/ssg-overview-gitlab-pages-part-2/)
  - [SSGs Part 3: Build any SSG site with GitLab Pages](https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/)
- [Posting to your GitLab Pages blog from iOS](https://about.gitlab.com/2016/08/19/posting-to-your-gitlab-pages-blog-from-ios/)

## Advanced Use

- Blog Posts
  - [GitLab CI: Run jobs Sequentially, in Parallel, or Build a Custom Pipeline](https://about.gitlab.com/2016/07/29/the-basics-of-gitlab-ci/)
  - [GitLab CI: Deployment & Environments](https://about.gitlab.com/2016/08/26/ci-deployment-and-environments/)
  - [Building a new GitLab Docs site with Nanoc, GitLab CI, and GitLab Pages](https://about.gitlab.com/2016/12/07/building-a-new-gitlab-docs-site-with-nanoc-gitlab-ci-and-gitlab-pages/)
  - [Publish Code Coverage Report with GitLab Pages](https://about.gitlab.com/2016/11/03/publish-code-coverage-report-with-gitlab-pages/)

## General Documentation

- [User docs](../user/project/pages/)
- [Admin docs](administration.html)
  - Video tutorial: [enable GitLab Pages to your GitLab instance (CE/EE)](#LINK)
